class serverAPI {
    request(endpointName, body) {
        return fetch('https://cors-anywhere.herokuapp.com/https://api-v3.igdb.com/' + endpointName, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'user-key': 'a7dc33f27c16616a34c2bc2366099996',
            },
            body: body
        });
    }

    async topGames() {
        let response = this.request("games", `fields id, name, cover.url, rating, genres.name;where rating>0;sort rating desc;`);
        let result = await response;
        let value = await result.json();
        return value
    }
    async gamesGenres() {
        let response = this.request("genres", `fields id, name; limit 30;`);
        let result = await response;
        let value = await result.json();
        return value
    };
    async filteredGames(genres, searchInput) {
        let request = "fields id, name, genres.name ,cover.url; limit 500;";
        if (searchInput) {
            request += `search "${searchInput}";`;
        }
        if (genres) {
            request += `where genres.name =(${genres});`;
        }
        let response = this.request("games",request);
        let result = await response;
        let value = await result.json();
        return value
    };

}

export {
    serverAPI
}