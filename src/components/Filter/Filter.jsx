import React, { Fragment } from "react";
import { connect } from "react-redux";
import Actions from "../../Redux/Actions";
import ActionsThunk from "../../Redux/ActionsThunk";
import { Button } from "../Button";
import { FilterListItem } from "../FilterListItem/FilterListItem";
import "./style.css";




class Filter extends React.Component {
    selectedFilters = new Set();
    inputRef = React.createRef();
    onFiltersStatusChange = (filterName, checked) => {
        if (checked)
            this.selectedFilters.add(`"${filterName}"`);
        else
            this.selectedFilters.delete(`"${filterName}"`);
    }
    onApply = () => {
        let searchInput = this.inputRef.current.value;
        let arrOfFilters = [...this.selectedFilters];
        if (searchInput == "" && arrOfFilters.length<1) {
            return alert("Заполните поле поиска или выберите жанр");
        }
        else {
            let genresReq = arrOfFilters.join();
            this.props.searchedGames(genresReq, searchInput);
        }
    }


    render() {
        return <Fragment>
            <div><input className="filterInput" placeholder="Enter Game Name" ref={this.inputRef} />
                <ul className="filter" >
                    {
                        this.props.genres ?
                            this.props.genres.map(genres => <FilterListItem
                                key={genres.id}
                                title={genres.name}
                                onStatusChange={(checked) => this.onFiltersStatusChange(genres.name, checked)}
                            />)
                            : null
                    }
                    <Button title="Search" onClick={this.onApply} />
                </ul>
            </div>
        </Fragment>

    }
}

function mapDispatchToProps(dispatch){
    return{
        searchedGames:(genresReq, searchInput)=>dispatch(ActionsThunk.searchedGames(genresReq, searchInput)),
    };
}


function mapStateToProps(state) {
    return {
        genres: state.get("genres"),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Filter)