import React from "react";
import "./style.css";
import { connect } from "react-redux";
import Actions from "../../Redux/Actions";


class Pagination extends React.Component {
    prevPage = (e) => {
        e.preventDefault();
        let prevPage = this.props.currentPage - 1;
        this.props.pageChangeTo(prevPage);
    }
    nextPage = (e) => {
        e.preventDefault();
        let nextPage = this.props.currentPage + 1;
        this.props.pageChangeTo(nextPage);

    }
    render() {
        return <ul className="pageList">
            <li>
                {
                    this.props.currentPage == 1 ?
                        "Prev"
                        : <a href="#" onClick={this.prevPage}>Prev</a>
                }
            </li>
            <li>{this.props.currentPage}/{this.props.pageCount}</li>
            <li>
                {
                    this.props.currentPage == this.props.pageCount ?
                        "Next"
                        : <a href="#" onClick={this.nextPage}>Next</a>
                }
            </li>
        </ul>
    }
}

function mapStateToProps(state) {
    return {
        currentPage: state.get("currentPage"),
        pageCount: state.get("pageCount")
    }
}

export default connect(mapStateToProps, Actions)(Pagination);