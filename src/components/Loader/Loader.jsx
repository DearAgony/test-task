import React from "react";
import "./style.css";
import {connect} from "react-redux";
import Actions from "../../Redux/Actions";


class Loader extends React.Component {

    render() {
        return <div className="loaderWrapper" data-loading={this.props.loading}>
                <div className="loaderBg"></div>
                <div className="loader"></div>
           
        </div>
    }
}

function mapStateToProps(state) {
    return {
        loading:state.get("loading"),
     }
    
}

export default connect(mapStateToProps, Actions)(Loader);