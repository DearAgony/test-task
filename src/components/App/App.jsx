
import React from "react";
import { EntrancePage } from "../EntrancePage/EntrancePage";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import GameListReducer from "../../Redux/Reducer";
import { BrowserRouter } from "react-router-dom";
import thunk from "redux-thunk";


const store = createStore(GameListReducer, applyMiddleware(thunk));

const App = (props) => <Provider store={store}>
    <BrowserRouter>
        <EntrancePage />
    </BrowserRouter>
</Provider>;

export {
    App
} 