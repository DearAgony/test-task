import React from "react";
import "./style.css";

class Game extends React.PureComponent {
    render() {
        return <div className="game" >
            <img src={this.props.cover} alt="нету картинки" />
            <p>Name: {this.props.name}</p>
            <p key={this.props.id}>Genre: {this.props.genres ? this.props.genres.map(genre=> <span key = {genre.id}> {genre.name}</span>) : "Without genre"}</p>
        </div>

    };
}
export default Game


