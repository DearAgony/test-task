import React from "react";
import "./style.css";
import Main from "../Main/Main";
import { Switch, Route, Link } from "react-router-dom";


class EntrancePage extends React.PureComponent {
    render() {
        return <Switch>
            <Route exact path="/">
                <Link to="/Main">
                    <div className="header">
                        <h2>Web App </h2>

                    </div>
                </Link>
                <p>This app can search games by name/genre, switch view type(cards, table, groups), you can use filter (genre for search)  and sort games by name. You can try search something like "creed" and check result  
                </p>
                <p>API:
                    <a href="https://www.igdb.com/api">https://www.igdb.com/api
                    </a>
                </p>
            </Route>
            <Route path="/Main" component={Main} />
        </Switch>;
    }
}

export {
    EntrancePage
}