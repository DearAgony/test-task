import React, { Fragment } from "react";
import GamesCards from "../GamesCards/GamesCards";
import Filter from "../Filter/Filter"
import Pagination from "../Pagination/Pagination";
import GamesByGenre from "../GamesByGenre/GamesByGenre";
import { Button } from "../Button";
import "./style.css";
import GamesTable from "../GamesTable/GamesTable";
import { connect } from "react-redux";
import Actions from "../../Redux/Actions";
import ActionsThunk from "../../Redux/ActionsThunk";
import Loader from "../Loader/Loader";




class Main extends React.Component { 
    constructor(props){
        super(props); 
        this.props.initLoad();
    }
   
    changeView = () => {
        this.props.nextViewType();
    }
    sortByName = () => {
        this.props.sortByName();
    }
    render() {
        return <Fragment>
            <Loader/>
            <Button className="changeView" title="Change view type" onClick={this.changeView} />
            <Button className="sortButton" title="Sort by Name" onClick={this.sortByName} />
            <div className="mainView"><Filter />
                {this.props.viewType == "Card" ?
                    <GamesCards /> :
                    this.props.viewType == "Table" ?
                        <GamesTable /> :
                        <GamesByGenre />}
            </div>
            {
                this.props.viewType == "GameByGenre" ?
                    null :
                    <Pagination />
            }

        </Fragment >

    }

}
function mapDispatchToProps(dispatch){
    return{
        nextViewType: ()=> dispatch(Actions.nextViewType()),
        sortByName:()=> dispatch(Actions.sortByName()),
        loadingDisable:()=>dispatch(Actions.loadingDisable()),
        loadingEnable:()=>dispatch(Actions.loadingEnable()),
        initLoad:()=>dispatch(ActionsThunk.initLoad())
    };
}

function mapStateToProps(state) {

    return {
        viewType: state.get("viewType"),

    };

}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
