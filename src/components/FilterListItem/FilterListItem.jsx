import React from "react";


class FilterListItem extends React.PureComponent {
    render() {
        return <li className="item">
            <input type="checkbox" onChange={(event) => this.props.onStatusChange(event.target.checked)} />
            {this.props.title}
        </li>
    }
}

export {
    FilterListItem
}