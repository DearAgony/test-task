import React from "react";
import { connect } from "react-redux";
import Actions from "../../Redux/Actions";
import Game from "../Game/Game";
import "./style.css";


class GamesByGenre extends React.PureComponent {
    
    render() {
        const categories = new Map();
        categories.set("Without genre", []);
        for(let i=0; i< this.props.games.length; i++){
            const game = this.props.games[i];
            if(game.genres) {
                for(let j=0; j < game.genres.length; j++){
                    let filteredGames = categories.get(game.genres[j].name);
                    if(!filteredGames){
                        filteredGames = [];
                        categories.set(game.genres[j].name, filteredGames);
                    }
                    filteredGames.push(game);
                }
            }
            else {
                categories.get("Without genre").push(game);
            }
        }
        if(categories.get("Without genre").length == 0)
            categories.delete("Without genre");
     
        return <ul className="gemesByGenre">
            {Array.from(categories).sort().map(([genre, games]) => <li key={genre}><div><h3>{genre}</h3>{
                games.map(game => <Game
                    key={game.id}
                    name={game.name}
                    cover={game.cover ? game.cover.url : null}
                    genres={game.genres} />)}</div></li>)
            } </ul>
    }
}

function mapStateToProps(state) {
    let take = state.get("maxGamesOnPage") * 50;
    let skip = 0;
    let gamesLeft = state.get("games").slice(skip, skip + take);
    let genres = state.get("genres");
    return {
        games: gamesLeft,
        genres: genres,
    }

}

export default connect(mapStateToProps, Actions)(GamesByGenre);