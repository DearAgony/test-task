import React from "react";
import {connect} from "react-redux";
import Actions from "../../Redux/Actions";
import Game from "../Game/Game";
import "./style.css";


class GamesTable extends React.PureComponent{ 
    render(){ 
        return <div className="gameTable">
            {
                this.props.games.map(game=><Game 
                    key ={game.id}
                    name={game.name}
                    cover={game.cover ? game.cover.url : null}
                    genres={game.genres}/>)
            }
        </div>
    }
}

function mapStateToProps(state) {
    let take = state.get("maxGamesOnPage");
    let skip = (state.get("currentPage")-1)*take;
    let gamesLeft = state.get("games").slice(skip, skip+take);
    return {
        games: gamesLeft,
     }
    
}

export default connect(mapStateToProps, Actions)(GamesTable);