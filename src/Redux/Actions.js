const showGames = function (showGames) {
    return {
        type: "SHOW_GAMES",
        showGames
    };
};
const showGenres = function(showGenres){
    return{
        type: "SHOW_GENRES",
        showGenres
    };
};
const pageChangeTo = function(index){
    return {
        type: "PAGE_CHANGE_TO",
        index
    };
};
const nextViewType = function(){
    return {
        type: "NEXT_VIEW_TYPE",
    };
};
const sortByName=function(){
    return{
        type: "SORT_BY_NAME",
    };
};
const loadingEnable=function(){
    return{
        type: "LOADING_ENABLE"
    };
};
const loadingDisable=function(){
    return{
        type: "LOADING_DISABLE"
    };
};
export default {
    showGames,
    pageChangeTo, 
    nextViewType,
    sortByName,
    showGenres,
    loadingDisable,
    loadingEnable
};