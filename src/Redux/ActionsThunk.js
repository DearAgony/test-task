import Actions from "./Actions";
import { serverAPI } from "../serverAPI";

const initLoad = function () {
    return function (dispatch) {
        dispatch(Actions.loadingEnable());
        let gamesPromise = new serverAPI().topGames().then((data) => {
            dispatch(Actions.showGames(data));
        });
        let genresPromise = new serverAPI().gamesGenres().then((genres) => {
            dispatch(Actions.showGenres(genres));
        });
        Promise.all([genresPromise, gamesPromise]).then(() => dispatch(Actions.loadingDisable()));
    }
}

const searchedGames = function (genresReq, searchInput) {
    return function (dispatch) {
        dispatch(Actions.loadingEnable());
        new serverAPI().filteredGames(genresReq, searchInput).then(
            (data) => {
                dispatch(Actions.showGames(data));
                dispatch(Actions.loadingDisable());
            });


    }
}




export default {
    initLoad,
    searchedGames
}

