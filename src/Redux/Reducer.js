import { Map } from "immutable";

const initState = {
    games: [],
    pageCount: 1,
    currentPage: 1,
    maxGamesOnPage: 10,
    genres: [],
    viewType: "Card",
    sorting: "ascending",
    loading: false,
};

const reducer = function (state = Map(initState), action) {
    switch (action.type) {
        case "SHOW_GAMES":
            return state.update("games", function (games) {
                return action.showGames;

            }).update("pageCount", function (pageCount) {
                let pages = Math.ceil(action.showGames.length / state.get("maxGamesOnPage"));
                if (pages == 0) {
                    pages = 1;
                    return pages;
                }
                return pages;
            });
        case "PAGE_CHANGE_TO":

            return state.update("currentPage", function () {
                if (action.index > state.get("pageCount")) {
                    action.index = state.get("pageCount")
                }
                else if (action.index < 1) {
                    action.index = 1
                }
                return action.index;

            })
        case "SHOW_GENRES":
            return state.update("genres", function (genres) {
                let res = action.showGenres.sort(function (a, b) {
                    let nameA = a.name.toLowerCase();
                    let nameB = b.name.toLowerCase();
                    if (nameA < nameB)
                        return -1;
                    if (nameA > nameB)
                        return 1;
                    return 0;
                })
                return res;
            })
        case "NEXT_VIEW_TYPE":
            return state.update("viewType", function () {
                let view = "Card";
                if (state.get("viewType") == "Card") {
                    view = "Table";
                }
                else if (state.get("viewType") == "Table") {
                    view = "GameByGenre";
                }
                return view;
            })
        case "SORT_BY_NAME":
            return state.update("games", function (games) {
                const sorting = state.get("sorting");
                for (let i = 1, l = games.length; i < l; i++) {
                    const current = games[i];
                    let j = i;
                    while (j > 0 && (sorting == "ascending" ? (games[j - 1].name > current.name) : (games[j - 1].name < current.name))) {
                        games[j] = games[j - 1];
                        j--;
                    }
                    games[j] = current;
                }
                return [...games];
            }).update("sorting", function (sorting) {
                return sorting == "ascending" ? "descending" : "ascending";
            });
        case "LOADING_ENABLE":
            return state.update("loading", ()=> true);
        case "LOADING_DISABLE":
            return state.update("loading", ()=> false);

    }

    return state;
}
export default reducer;