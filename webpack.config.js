const htmlWebpackPlugin= require('html-webpack-plugin');
const path = require('path');
module.exports={
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        compress: true,
        port: 9000
    },
    mode: "development",
    entry: "./src/index.jsx",
    output: {
        path: __dirname + "/public"
    },
    resolve: {
        extensions:[".js", ".jsx"]
    },
    plugins: [new htmlWebpackPlugin({template:"./src/index.html"})],
    module:{
        rules: [{
            test:/\.jsx?$/,
            exclude: /node_modules/,
            use: {
                loader:"babel-loader"
            }
        },{
            test:/\.css$/,
            exclude: /node_modules/,
            use: [
                "style-loader",
                "css-loader"
               
            ]
        }],
    }
}